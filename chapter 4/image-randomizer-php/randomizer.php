<!DOCTYPE html >
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Creating a PHP-based image randomizer</title>
<style type="text/css" title="text/css">
    /* <![CDATA[ */
    @import url(randomizer.css);
    /* ]]> */
</style>
</head>
<body>
<?php
$picarray = array("stream" => "A photo of a stream", "river" => "A photo of a river", "road" => "A photo of a road");
$randomkey = array_rand($picarray);

echo '<img src="assets/random-images/'.$randomkey.'.jpg"  alt="'.$picarray[$randomkey].'" width="300" height="300" class="addBorder" />';
echo '<p>'.$picarray[$randomkey].'</p>';
?>
</body>
</html>
